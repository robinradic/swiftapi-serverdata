package org.radic.minecraft.swiftapi.serverdata;

import org.apache.thrift.TProcessor;
import org.phybros.minecraft.Api;
import org.phybros.minecraft.extensions.SwiftExtension;
import org.radic.minecraft.swiftapi.serverdata.commands.ServerDataCommand;
import org.radic.minecraft.swiftapi.serverdata.handlers.ServerDataApiHandler;
import org.radic.minecraft.swiftapi.serverdata.thrift.SwiftApiServerData;

public class ServerDataExtension extends SwiftExtension {

    public void enable() {
        Api.debug("Haai");
    }

    public void disable() {
        Api.debug("Baai");
    }

    @Override
    public void register() {
        Api.debug("register");
        registerCommand("serverdata", new ServerDataCommand());
        registerApiHandler("org.radic.serverdata.serverdata.thrift.SwiftApiServerData");
        registerConfig("serverdata", "config")
                .section("logging")
                .bool("logging.enabled")
                .string("logging.file");

        registerConfig("serverdata", "worlds")
                .string("type");
    }

    @Override
    @SuppressWarnings("unchecked")
    public TProcessor getApiProcessor(String name) {
        Api.debug("ServerDataExtension.getApiProcessor", name);
        if (name.equals("SwiftApiServerData")) {
            return new SwiftApiServerData.Processor(new ServerDataApiHandler());
        }
        return null;
    }

}
