package org.radic.minecraft.swiftapi.serverdata.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

import org.phybros.minecraft.Api;
import org.phybros.minecraft.SwiftApiPlugin;
import org.phybros.minecraft.commands.ICommand;

import java.io.File;

public class ServerDataCommand implements ICommand
{

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args, SwiftApiPlugin plugin) {

        Api.message(sender, "Ok nice!");

        Api.message(sender, "Free memory (bytes)", String.valueOf(Runtime.getRuntime().freeMemory()));

        long maxMemory = Runtime.getRuntime().maxMemory();
        Api.message(sender, "Maximum memory (bytes)", String.valueOf(maxMemory == Long.MAX_VALUE ? "no limit" : maxMemory));


        Api.message(sender, "Total memory (bytes)", String.valueOf(Runtime.getRuntime().totalMemory()));

        Api.message(sender, "Available processors", String.valueOf(Runtime.getRuntime().availableProcessors()));
        /* Get a list of all filesystem roots on this system */
        File[] roots = File.listRoots();

        /* For each filesystem root, print some info */
        for (File root : roots) {
            Api.message(sender, "File system root: " + root.getAbsolutePath());
            Api.message(sender, "Total space (bytes): " + root.getTotalSpace());
            Api.message(sender, "Free space (bytes): " + root.getFreeSpace());
            Api.message(sender, "Usable space (bytes): " + root.getUsableSpace());
        }


        return true;
    }


}