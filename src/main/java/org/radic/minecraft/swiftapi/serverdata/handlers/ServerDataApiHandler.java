package org.radic.minecraft.swiftapi.serverdata.handlers;

import org.apache.thrift.TException;
import org.phybros.minecraft.extensions.SwiftApiHandler;
import org.phybros.thrift.EAuthException;
import org.phybros.thrift.EDataException;
import org.radic.minecraft.swiftapi.serverdata.SysInfo;
import org.radic.minecraft.swiftapi.serverdata.thrift.*;


import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class ServerDataApiHandler extends SwiftApiHandler implements SwiftApiServerData.Iface {

    private SysInfo si;
    public ServerDataApiHandler(){
        si = SysInfo.getInstance();
    }

    public Size makeSize(long bytes) {
        Size size = new Size();
        size.prettyBits = si.prettifyBytes(bytes, true);
        size.prettyBytes = si.prettifyBytes(bytes, false);
        size.KB = ((int) bytes / 1024);
        size.MB = ((int) bytes / 1024 / 1024);
        size.GB = ((int) bytes / 1024 / 1024 / 1024);
        return size;
    }

    public MemoryInfo getMemoryInfo() {
        MemoryInfo memory = new MemoryInfo();
        long[] ram = si.getRam(SysInfo.Size.B);
        memory.free = makeSize(ram[0]);
        memory.max = makeSize(ram[1]);
        memory.used = makeSize(ram[2]);
        memory.percent = ram[3];
        return memory;
    }

    public List<DiskInfo> getFileSystems() {
        List<DiskInfo> fs = new ArrayList<>();
        for (Map.Entry<String, long[]> entry : si.getFileSystems().entrySet()) {
            long[] sizes = entry.getValue();
            fs.add(new DiskInfo(
                    entry.getKey(),
                    makeSize(sizes[0]),
                    makeSize(sizes[1]),
                    makeSize(sizes[2])
            ));
        }
        return fs;
    }

    public OsInfo getOsInfo(){
        OsInfo os = new OsInfo();
        os.name = si.osName;
        os.version = si.osVersion;
        return os;
    }

    public JavaInfo getJavaInfo(){
        JavaInfo ji = new JavaInfo();
        ji.name = si.javaRuntimeName;
        ji.version = si.javaRuntimeVersion;
        return ji;
    }


    @Override
    public ServerInfo getServerInfo(String authString) throws EAuthException {
        logCall("getServerInfo");
        authenticate(authString, "getServerInfo");
        ServerInfo s = new ServerInfo();
        s.memory = getMemoryInfo();
        s.fileSystems = getFileSystems();
        s.processor = new CpuInfo();
        s.os = getOsInfo();
        s.jvm = getJavaInfo();
        return s;
    }

    @Override
    public PerformanceInfo getPerformanceInfo(String authString) throws EAuthException, EDataException, TException {
        logCall("getPerformanceInfo");
        authenticate(authString, "getPerformanceInfo");

        PerformanceInfo p = new PerformanceInfo();

        p.memory = getMemoryInfo();
        p.processor = new CpuInfo();
        p.fileSystems = getFileSystems();

        return p;
    }

}
