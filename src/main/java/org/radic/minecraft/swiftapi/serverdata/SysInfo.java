package org.radic.minecraft.swiftapi.serverdata;


import java.io.File;
import java.util.HashMap;

/**
 * Created by radic on 7/30/14.
 */
public class SysInfo {

    private Runtime runtime;
    private static SysInfo instance = null;

    public static SysInfo getInstance() {
        if (instance == null) {
            instance = new SysInfo();
        }
        return instance;
    }
    public enum Size {
        B(1), KB(1024), MB(1048576), GB(1073741824);

        private int code;

        private Size(int c) {
            code = c;
        }

        public int getSize() {
            return code;
        }
    }

    public String prettifyBytes(long bytes, boolean si) {
        int unit = si ? 1000 : 1024;
        if (bytes < unit) return bytes + " B";
        int exp = (int) (Math.log(bytes) / Math.log(unit));
        String pre = (si ? "kMGTPE" : "KMGTPE").charAt(exp-1) + (si ? "" : "i");
        return String.format("%.1f %sB", bytes / Math.pow(unit, exp), pre);
    }

    public String osArch;
    public String osVersion;
    public String osName;

    public String javaRuntimeName;
    public String javaRuntimeVersion;

    public String fileSeperator;

    public int availableProcessors;

    private SysInfo() {

        runtime = Runtime.getRuntime();
        osArch = System.getProperty("os.arch");
        osName = System.getProperty("os.name");
        osVersion = System.getProperty("os.version");

        javaRuntimeName = System.getProperty("java.runtime.name");
        javaRuntimeVersion = System.getProperty("java.runtime.version");

        fileSeperator = System.getProperty("file.seperator");

        availableProcessors = runtime.availableProcessors();
    }

    public long getRamFree(Size size){
        return getRamMax(size) - getRamUsed(size);
    }

    public long getRamMax(Size size){
        return runtime.maxMemory() / size.getSize();
    }

    public long getRamTotal(Size size){
        return runtime.totalMemory() / size.getSize();
    }

    public long getRamUsed(Size size){
        return getRamTotal(size);
    }

    public float getRamUsedPercent(){
        return ((float) getRamUsed(Size.B) / getRamMax(Size.B)) * 100;
    }

    public long getRamUsedPercentRounded(){
        return Math.round(getRamUsedPercent());
    }

    /**
     * Get the current memory usage
     * @param size te size
     * @return array = { free, max, total, percent }
     */
    public long[] getRam(Size size) {
        return new long[]{ getRamFree(size), getRamMax(size), getRamTotal(size), getRamUsedPercentRounded() };
    }

    public HashMap<String, long[]> getFileSystems() {
        HashMap<String, long[]> fileSystems = new HashMap<>();
        File[] roots = File.listRoots();
        for (File root : roots) {
            fileSystems.put(root.getAbsolutePath(), new long[] { root.getTotalSpace(), root.getFreeSpace(), root.getUsableSpace() });
        }
        return fileSystems;
    }




}
