include '../SwiftApi/SwiftApi.thrift'

namespace java org.radic.minecraft.swiftapi.serverdata.thrift
namespace csharp org.radic.minecraft.swiftapi.serverdata.thrift
namespace php Phybros.SwiftApi.Extensions.ServerData.thrift

struct Size {
    1:i32 KB,
    2:double MB,
    3:double GB,
    4:string prettyBits,
    5:string prettyBytes
}

struct MemoryInfo {
    1:Size free,
    2:Size max,
    3:Size used,
    4:double percent,
}


struct CpuInfo {
    1:i32 amount,
    2:string name,
    3:i16 arch
}

struct DiskInfo {
    1:string path,
    2:Size total,
    3:Size free,
    4:Size usable,
}

struct JavaInfo {
    1:string name,
    2:string version,
}

struct OsInfo {
    1:string name,
    2:string version,
}

struct ServerInfo {
    1:MemoryInfo memory,
    2:list<DiskInfo> fileSystems,
    3:CpuInfo processor,
    4:OsInfo os,
    5:JavaInfo jvm,
}

struct PerformanceInfo {
    1:MemoryInfo memory,
    2:list<DiskInfo> fileSystems,
    3:CpuInfo processor,
}


service SwiftApiServerData {
    ServerInfo getServerInfo(1:string authString) throws(1:SwiftApi.EAuthException eax),
    PerformanceInfo getPerformanceInfo(1:string authString) throws(1:SwiftApi.EAuthException eax, 2:SwiftApi.EDataException dex),
}
